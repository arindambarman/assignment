
// Servives
var FileService = require('./services/file-service');
var Utility = require('./services/utility-service');


// Private Methods
function validateInputParameters() {
  // We don't validate the customer list because it is an external file and
  // we need to handle external errors more gracefully.
  if (!this.originCoordinates) {
    throw new Error('Origin coordinates are not provided')
  } else if (!Utility.validateLatLong(this.originCoordinates)) {
    throw new Error ('Origin coordinates provided are not valid')
  } else if (!Utility.validateRadiusLimit(this.radiusLimit)) {
    throw new Error('Radius Limit is not valid')
  }
}


function isCustomerWithinRadius(customer) {

  let customerCoordinates = { lat: customer.latitude, long: customer.longitude };
  var distance = Utility.distanceBetweenPoints(customerCoordinates, this.originCoordinates)
  // Add a validation if distance is not a valid number return false;
  if (isNaN(distance)) {
    return;
  }
  return distance <= this.radiusLimit
}

// public Methods
Internals.prototype.generateInvitationList = function (callback) {

  FileService.readFromTextFile(this.fileLocation,  (err, data) => {

    if (err) {
      throw new Error(err);
    }

    var validInvitations = [];
    data.forEach((customer) => {

      if (isCustomerWithinRadius.call(this, customer)) {

        validInvitations.push(customer);
      }
    })

    return callback(null, validInvitations);
  })
}

var Internals = function (originCoordinates, fileLocation, radiusLimit) {


  // originCoordinates should be a json containing lat and long coordinates.
  // Example: { lat: '10', long: '10' }
  // fileLocation should be the location of the customers json file.
  // if any of them are not initialized we'll throw error
  this.originCoordinates = originCoordinates;
  this.fileLocation = fileLocation;
  this.radiusLimit = radiusLimit;
  validateInputParameters.call(this);
};



module.exports = Internals;
