
var fs = require('fs');
var internals = {};

// private methods
function convertTextToArray(data) {

  return data.split('\n');
}

function isObject(obj) {

  return obj === Object(obj) && Object.prototype.toString.call(obj) !== '[object Array]'
}

function validatedArray(data) {

  // We need to validate if each item in the array is a valid JSON or not.
  // Otherwise we remove it from the array.
  var validatedArray = [];
  data.forEach((item) => {

    try {
      var parsedJSON = JSON.parse(item);
      // We will have to validate if the entry is a hashmap or not
      if (isObject(parsedJSON)) {
        validatedArray.push(parsedJSON)
      }
    } catch (_e) {
      // Do not push item to array
    }
  })
  return validatedArray;
}

// public methods
internals.readFromTextFile = function(fileLocation, callback) {

  // Format of text file should be lines of JSON. Each new JSON is assumed to be
  // in a new line. Example:
  // { foo: 'value 1' }
  // { foo: 'value 2'}
  fs.readFile(fileLocation, 'utf8', function (err, data) {

    if (err) {
       return callback('Error reading file.');
    }

    // convert text to array
    var jsonArray = convertTextToArray(data);
    var validatedJSONs = validatedArray(jsonArray);
    return callback(null, validatedJSONs);
  });
}

module.exports = internals;
