
var internals = {};
const EARTH_RADIUS = 6371;

internals.degreesToRadians = function (value) {

  // validation check if not number return null;
  if (isNaN(Number(value))) {
    return;
  }

  return Number(value) * (Math.PI / 180);
}

internals.validateRadiusLimit = function (radiusLimit) {

  return !isNaN(Number(radiusLimit))
}

internals.validateLatLong = function (coordinates) {

  return (!isNaN(Number(coordinates.lat)) && !isNaN(Number(coordinates.long)))
}

internals.distanceBetweenPoints = function (coordinates1, coordinates2) {

  if (!internals.validateLatLong(coordinates1) || !internals.validateLatLong(coordinates2)) {
    return;
  }

  var c1RadLat = internals.degreesToRadians(coordinates1.lat);
  var c1RadLong = internals.degreesToRadians(coordinates1.long);
  var c2RadLat = internals.degreesToRadians(coordinates2.lat);
  var c2RadLong = internals.degreesToRadians(coordinates2.long);

  var deltaAngle = Math.acos(Math.sin(c1RadLat) * Math.sin(c2RadLat) +
    Math.cos(c1RadLat) * Math.cos(c2RadLat) * Math.cos(c1RadLong - c2RadLong))

  var distance = deltaAngle * EARTH_RADIUS;
  return distance;
}

module.exports = internals;
