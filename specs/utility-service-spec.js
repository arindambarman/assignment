
// Test cases for utility service.js
var UtilityService = require('../services/utility-service');

describe("Utility Specs", function() {

  it("tests that utility service is defined", function() {

    expect(UtilityService).toBeDefined();
  });

  it('calculates the distance between points correctly', function () {

    var coords_1 = { lat: '52.833502', long: '-8.522366' };
    var coords_2 = { lat: '53.339428', long: '-6.257664' };
    var distance = UtilityService.distanceBetweenPoints(coords_1, coords_2);
    expect(distance).toBeCloseTo(161.4, 1);
  });

  it('handles errors gracefully if input coordinates are incorrectly sent', function() {

    var coords_1 = { lat: '52.1', long: '-8.5' };
    var coords_2 = { lat: 'abc', long: '-8.5' };
    var distance = UtilityService.distanceBetweenPoints(coords_1, coords_2);
    expect(distance).toBeUndefined();
  });

  it('correctly converts degree to radian', function () {

    var degree = 50;
    expect(UtilityService.degreesToRadians(degree)).toBeCloseTo(0.87, 2);
  });

  it('gracefully handles error while converting degree to radian', function () {

    var degree = 'foo';
    expect(UtilityService.degreesToRadians(degree)).toBeUndefined();
  });
})
