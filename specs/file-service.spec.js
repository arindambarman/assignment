
// Test cases for file service.js
var FileService = require('../services/file-service');

describe("Files Specs", function() {

  it("tests that file service is defined", function() {

    expect(FileService).toBeDefined();
  });

  it("tests that a text file is properly read and converted to JSON", function() {

    FileService.readFromTextFile('./helpers/mock-file.txt', function (err, data) {

      expect(err).toBeNull();
      expect(data.length).toEqual(2);
      expect(data[0].foo).toEqual('bar');
      expect(data[1].foo).toEqual('bar_1');
    })
  })

  it("tests that a text file correctly parses JSON items only", function() {

    FileService.readFromTextFile('./helpers/mock-file-invalid-json.txt', function (err, data) {

      expect(err).toBeNull();
      expect(data.length).toEqual(2);
      expect(data[0].foo).toEqual('bar');
      expect(data[1].foo).toEqual('bar_1');
    })
  })

  it("tests that the methods return err when file doesn't exist", function() {

    FileService.readFromTextFile('./helpers/mock-file-non-existent.txt', function (err, data) {

      expect(err).toBe('Error reading file.')
    })
  })
})
