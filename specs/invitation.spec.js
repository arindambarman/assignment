
// Test cases for main module

const MOCK_FILE_LOCATION = './helpers/mock-file.txt';
const CUSTOMER_DATA = './helpers/customer-json.txt'
const DUBLIN_OFFICE = { lat: '53.339428', long: '-6.257664' };
const RADIUS_LIMIT = 100; //km
var Invitation = require('../invitations');

describe("Invitation Intitialization", function () {

  it("tests that invitations is defined", function() {

    expect(Invitation).toBeDefined();
  });

  it("Successfully returns customers eligibile to be invited", function () {

    var invitation = new Invitation(DUBLIN_OFFICE, CUSTOMER_DATA, RADIUS_LIMIT);
    invitation.generateInvitationList(function (err, data) {

      expect(err).toBeNull();
      expect(data.length).toEqual(16);
    })
  });

  it("throws error when invitation is not intitalized with origin coordinates", function () {

    expect(function () {
      new Invitation(undefined, MOCK_FILE_LOCATION, 100)
    }).toThrowError('Origin coordinates are not provided');
  });

  it("throws error when invitation is wrongly intitalized with origin coordinates", function () {

    expect(function () {
      new Invitation({ lat: '10', long: 'adsfasdf' }, MOCK_FILE_LOCATION, 100)
    }).toThrowError('Origin coordinates provided are not valid');
  });

  it("throws error when invitation is not intitalized with radius limit", function () {

    expect(function () {
      new Invitation({ lat: '10', long: '10' }, MOCK_FILE_LOCATION)
    }).toThrowError('Radius Limit is not valid');
  });

  it("throws error when invitation is wrongly intitalized with radius limit", function () {

    expect(function () {
      new Invitation({ lat: '10', long: '10' }, MOCK_FILE_LOCATION, 'asdfasd')
    }).toThrowError('Radius Limit is not valid');
  });
})

